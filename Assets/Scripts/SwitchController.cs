﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class SwitchController : MonoBehaviour
    {
        public delegate void InputCharAction();
        public static event InputCharAction InputChar;

        public Text SwitchMessage;
        
        private bool _isSwitch;
        private bool _isVisits;

        // Use this for initialization
        private void Start()
        {
            SwitchMessageText(null);
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("SwitchTriggerEnter");
            if (other.gameObject.CompareTag("Player") && _isSwitch == false)
            {
                SwitchMessageText("Press e to discover a path!");
                _isVisits = true;
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (Input.GetKeyDown(KeyCode.E) && _isVisits)
            {
                Debug.Log("SwitchTriggerStay");
                SwitchMessageText("You discovered a path!");
                _isSwitch = true;
                if (InputChar != null)
                {
                    InputChar();
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            Debug.Log("SwitchTriggerExit");
            _isVisits = false;
            SwitchMessageText(null);
        }

        private void SwitchMessageText(string msg)
        {
            SwitchMessage.text = msg;
        }
    }
}
