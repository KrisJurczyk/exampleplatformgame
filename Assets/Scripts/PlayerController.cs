﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class PlayerController : MonoBehaviour
    {
        public Text WelcomeText;
        public Text TimeRecording;

        private float _deathHeight = -8;
        private float _startTime;

        // Use this for initialization
        private void Start()
        {
            TimeStart();
            WelcomeText.text = "Find a red buoy!";
            Scene scene = SceneManager.GetActiveScene();
            Debug.Log("Active scene is '" + scene.name + "'.");
        }

        // Update is called once per frame
        private void Update()
        {
            if (Time.time - _startTime > 2)
            {
                WelcomeText.text = null;
            }
            TimeRecording.text = Math.Round(Time.time - _startTime, 2).ToString(CultureInfo.InvariantCulture);

            if (transform.position.y <= _deathHeight)
            {
                Respown();
            }
        }

        private void Respown()
        {
            Debug.Log("Responw");
            transform.position = Vector3.zero;
            TimeStart();
        }

        private void TimeStart()
        {
            _startTime = Time.time;
        }
    }
}
