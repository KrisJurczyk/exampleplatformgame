﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class FinishPlatformController : MonoBehaviour
    {
        public Text LevelWasFinished;
        
        // Use this for initialization
        private void Start()
        {
            LevelWasFinished.text = null;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                LevelWasFinished.text = "Gratulacje!";
                StartCoroutine(WaiterYield(5));
            }
        }

        private IEnumerator WaiterYield(int howLong)
        {
            yield return new WaitForSeconds(howLong);
            SceneManager.LoadScene("Level00");
        }
    }
}
