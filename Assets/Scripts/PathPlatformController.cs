﻿using UnityEngine;

namespace Assets.Scripts
{
    public class PathPlatformController : MonoBehaviour
    {
        private bool IsVisible { get; set; }

        // Use this for initialization
        private void OnEnable()
        {
            SwitchController.InputChar += PathActivated;
            ChangeComponents();
        }

        private void OnDisable()
        {
            SwitchController.InputChar -= PathActivated;
        }

        // Update is called once per frame
        private void Update()
        {
            ChangeComponents();
        }

        public void PathActivated()
        {
            IsVisible = true;
            Debug.Log("Event works");
        }

        private void ChangeComponents()
        {
            gameObject.GetComponent<Renderer>().enabled = IsVisible;
            gameObject.GetComponent<Collider>().enabled = IsVisible;
        }
    }
}
